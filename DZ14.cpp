﻿// DZ14.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <ctime>
using namespace std;

int main()
{
    // Это первая часть задания 

   const int N = 2;
    int array[N][N] = { {0, 1}, {2, 3} };
    for (int i = 0; i < N; ++i )
    {
        for (int j = 0; j < N; ++j)
        {  
            array[i][j] = (i + j);
            cout << array[i][j];
        }
        cout << "\n";
    }
    // Вторая часть задания 
    int sum = 0;
   const time_t t = time(NULL);
   struct tm* lt = localtime(&t);
   int day = lt -> tm_mday;
    int k = day % N;
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++) {
            cout << array[i][j];
            if (i == k) sum += array[i][j];
        }
    cout << "Сумма элементов строки " << k << " = " << sum;
    for (int i = 0; i < N; i++)
    return 0;
}